//
//  main.swift
//  Problem2
//
//  Created by Ismael F. Hepp on 2016-10-17.
//  Copyright © 2016 Ismael F. Hepp. All rights reserved.
//
// Project:      fn-assign3-2016
// File:         Problem2.swift
// Author:       Ismael F. Hepp
// Date:         Oct 18, 2016
// Course:       IMG204
//
//
// Problem Statement:
// Design and implement a program that counts the number of punctuation marks
// in a text input file.
// 1) Produce a table that shows how many times each symbol occurred.
// 2) Use a closure to list all words sorted alphabetically.
// 3) Use a closure to list all words sorted by their 2nd character.
// Inputs:   a file "data.txt" is used as an input
// Outputs:  a table showing number of occurrences of each punctuation mark

import Foundation

let str: String = "`~!@#$%^&*()_+-={}|[]\\:\";',./<>?"
let analiser: TextAnalyzer = TextAnalyzer(sentence: str)

print(analiser.showPunctuationTable())

let dataFile: String = IOLibrary.getFileContentAsStringFrom(
  folder: "", andFile: "data.txt")
let fileAnaliser: TextAnalyzer = TextAnalyzer(sentence: dataFile)

print(fileAnaliser.showPunctuationTable())

fileAnaliser.createWords()

for word in fileAnaliser.sortAlphabetically() {
  print(word)
}

for word in fileAnaliser.sortBy2ndCharacter() {
  print(word)
}


