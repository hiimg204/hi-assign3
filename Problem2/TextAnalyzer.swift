//
//  TextAnalyzer.swift
//  hi-assign3
//
//  Created by Student on 2016-10-17.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class TextAnalyzer {
  
  let punctuation = "`~!@#$%^&*()_-+={[}]|\\;:\"'<,>.?/"
  
  var sentence: String = ""

  private var words: [String] = []
  
  public init (sentence: String) {
    self.sentence = sentence
  }
  
  func by2ndChar(strOne:String, strTwo:String) -> Bool {
    var secondChar1:Character = " "
    var secondChar2:Character = " "
    
    if strOne.characters.count > 1 {
      secondChar1 = strOne[strOne.index(strOne.startIndex,offsetBy: 1)]
    } else if strOne.characters.count > 0 {
      secondChar1 = strOne.characters.first!
    }
    
    if strTwo.characters.count > 1 {
      secondChar2 = strTwo[strTwo.index(strTwo.startIndex,offsetBy: 1)]
    } else if strTwo.characters.count > 0 {
      secondChar2 = strTwo.characters.first!
    }
    
    return secondChar1 < secondChar2
  
  }
  
  public func createWords(){
    self.words = []
    let tmpWords = sentence.components(separatedBy:" ").filter{ $0.characters.count > 0 }
    for word in tmpWords {
      var tmpWord = ""
      for myChar in  word.characters {
        if myChar != "\n" {
          tmpWord = tmpWord + String(myChar)
        }
      }
      words.append(tmpWord)
    }
  }
  
  func sortAlphabetically() -> [String] {
    return words.sorted()
  }
  
  func sortBy2ndCharacter() -> [String] {
    return words.sorted(by: by2ndChar)
  }
  
  public func showPunctuationTable() -> String {
    var myPunctuation = [Character: Int]()
    
    for myChar in sentence.characters {
      if punctuation.characters.contains(myChar) {
        if myPunctuation[myChar] != nil {
          myPunctuation[myChar] = myPunctuation[myChar]! + 1
        }
        else {
          myPunctuation[myChar] = 1
        }
      }
    }
    
    var myTable: String = ""
    for (punctuation, count) in myPunctuation {
      myTable += "\(punctuation) : \(count)\n"
    }
    
    return myTable

  }
}
