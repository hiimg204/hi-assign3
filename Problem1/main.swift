//
//  main.swift
//  Problem1
//
//  Created by Student on 2016-10-17.
//  Copyright © 2016 Student. All rights reserved.
//

// Project:      hi-assign3
// File:         Problem1.swift
// Author:       Ismael F. Hepp
// Date:         Oct 18, 2016
// Course:       IMG204
//
// Problem Statement:
// Create a PalindromeTester program so that the spaces, punctuation, 
// and changes in uppercase and lowercase are not considered when 
// determining whether a string is a palindrome.
// Your solution must use main.swift plus 2 more classes.
// Hint: These issues can be handled in several ways. Think carefully about
// your design.
//
// Inputs:  a user enters a string
// Outputs: confirmation whether the input string is a palindrome

import Foundation

print("Enter a word:")
let str: String = IOLibrary.getConsoleInputLine()

print("You have entered: \(str)")

let strUtil: StringUtility = StringUtility()

let cleanString: String = strUtil.toLowerCase(string: str)

let palindrome: PalindromeTester = PalindromeTester(string: cleanString)

print(palindrome)
