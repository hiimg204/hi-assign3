//
//  PalindromeTester.swift
//  hi-assign3
//
//  Created by Student on 2016-10-17.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class PalindromeTester : CustomStringConvertible {
  
  private var string: String = ""
  
  public init(string: String) {
    self.string = string
  }
  
  public func isPalindrome() -> Bool {
    if string == String(string.characters.reversed()) {
      return true
    }
    else {
      return false
    }
  }
  
  var description: String {
    
    var description: String = ""
    
    if (self.isPalindrome()) {
      description = "\(string) is a Palindrome\n"
    }
    else {
      description = "\(string) is NOT a Palindrome\n"
    }
    
    return description
    
  }
  
  
}
