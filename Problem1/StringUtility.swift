//
//  StringUtility.swift
//  hi-assign3
//
//  Created by Frank  Niscak on 2016-10-03.
//  Copyright © 2016 NIC. All rights reserved.
//

import Foundation

class StringUtility {
  
  public func toLowerCase(string:String) -> String {
    
    let alphabet: String = "abcdefghijklmnopqrstuvwxyz"
    var cleanStr: String = ""
    
    for char in string.localizedLowercase.characters {
      if alphabet.characters.contains(char) {
        cleanStr += String(char)
      }
    }
    
    return cleanStr
  
  }
}
